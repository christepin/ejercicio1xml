
package com.christianfokoua.almacenarjuegos;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author chris
 */
public class GestionXml {
    
 private  DocumentBuilderFactory factory;     
 private DocumentBuilder dBuilder;
 

 private Document document;
 
 private String stringRoot;
 private String nombreFicheroXml;

 
 private Source source;
 private Result result;
 private Transformer transformer;
 private Element root;
 

    public GestionXml(String nombreFichero, String raizXml) {
     try {
         stringRoot = raizXml;
         nombreFicheroXml = nombreFichero;
         
         factory = DocumentBuilderFactory.newInstance();
         dBuilder = factory.newDocumentBuilder();
         document = dBuilder.newDocument();
         
         //raiz
         root = document.createElement(stringRoot);
         document.appendChild(root);
         
     } catch (ParserConfigurationException ex) {
         Logger.getLogger(GestionXml.class.getName()).log(Level.SEVERE, null, ex);
     }
    }
    
    public Element addElement(String stringElement){
        
        Element eHijo = document.createElement(stringElement);
        root.appendChild(eHijo);
        return eHijo;
    }
    
    public Element addElement(String stringElement, Element ePadre){
        
       Element eHijo = document.createElement(stringElement);
       ePadre.appendChild(eHijo); 
       return eHijo;
    }
    
    public void addAttribut(String stringAttr, String valor , Element eAttr){
       
        Attr attr = document.createAttribute(stringAttr);
        attr.setValue(valor);
        eAttr.setAttributeNode(attr);
        
    }
 
    public void addValue(String valor, Element e , Element ePadre){
        e.appendChild(document.createTextNode(valor));
        ePadre.appendChild(e);
    }
    
    
    
    
    
    
    public void xFormerXml(){
     try {
         source = new DOMSource(document);
         
         result = new StreamResult(new File(nombreFicheroXml));
         transformer = TransformerFactory.newInstance().newTransformer();
         transformer.transform(source, result);
         System.out.println("Xformacion realizado con exit!");
     } catch (TransformerConfigurationException ex) {
         Logger.getLogger(GestionXml.class.getName()).log(Level.SEVERE, null, ex);
     } catch (TransformerException ex) {
         Logger.getLogger(GestionXml.class.getName()).log(Level.SEVERE, null, ex);
     }
    }
 
    
    
 
    

      

             
        
      
           
       
    
    
}
