package com.christianfokoua.almacenarjuegos;

import org.w3c.dom.Element;




/**
 *
 * @author willy christian fokoua
 */
public class classMain {
    
    public static void main(String[] args){
        
  
        GestionXml gst = new GestionXml("Juego.xml", "listadejuegos");
        
        Juego j1 = new Juego("Super Mario Bross", "Multiplataforma", "NES", 1957);
        String st1 = Integer.toString(j1.getFechaLanzamiento());
        
        Juego j2 = new Juego("Resident Evil 6", "Horror", "PC", 2017);
        String st2 = Integer.toString(j2.getFechaLanzamiento());
    //---------------------------------------------------------------------------------------------------

    Element juego1 = gst.addElement("juego");
    
    Element titulo = gst.addElement("titulo", juego1);
    gst.addValue(j1.getTitulo(),titulo , juego1);
    
    Element genero = gst.addElement("genero", juego1);
    gst.addValue(j1.getGenero(),genero , juego1);
    
    Element plataforma = gst.addElement("plataforma", juego1);
    gst.addValue(j1.getPlataforma(),plataforma , juego1);
    
    Element fecha = gst.addElement("fecha", juego1);
    gst.addValue(st1,fecha , juego1);
    //--------------------------------------------------------------------------------------------------  
    
    Element juego2 = gst.addElement("juego");
    
    Element titulo2 = gst.addElement("titulo", juego2);
    gst.addValue(j2.getTitulo(), titulo2, juego2);
    
    Element genero2 = gst.addElement("genero", juego2);
    gst.addValue(j2.getGenero(), genero2, juego2);
    
    Element plataforma2 = gst.addElement("plataforma", juego2);
    gst.addValue(j2.getPlataforma(), plataforma2, juego2);
    
    Element fecha2 = gst.addElement("fecha", juego2);
    gst.addValue(st2, fecha2, juego2);
 
    //---------------------------------------------------------------------------------------------------   
        gst.xFormerXml();
        
      
    }
    
}
